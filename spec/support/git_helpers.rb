# frozen_string_literal: true

# Source: https://github.com/chef/omnibus/blob/master/spec/support/git_helpers.rb
module GitHelpers
  def clean_remote_git_repo(name)
    remote_path = File.join(remotes, name)
    local_path = git_scratch

    system("rm -rf #{remote_path} #{local_path}")
  end

  def git(command)
    time = Time.at(680_227_200).utc.strftime('%c %z')
    env  = {
      'GIT_AUTHOR_NAME' => 'omnibus',
      'GIT_AUTHOR_EMAIL' => 'omnibus@getchef.com',
      'GIT_AUTHOR_DATE' => time,
      'GIT_COMMITTER_NAME' => 'omnibus',
      'GIT_COMMITTER_EMAIL' => 'omnibus@gechef.com',
      'GIT_COMMITTER_DATE' => time
    }

    system(env, "git #{command}")
  end

  def git_scratch
    path = File.join(tmp_path, 'git', 'scratch')
    FileUtils.mkdir_p(path) unless File.directory?(path)
    path
  end

  def local_git_repo(name, options = {})
    path = git_scratch
    Dir.chdir(path) do
      # Create a bogus configure file
      File.open('configure', 'w') { |f| f.write('echo "Done!"') }

      git %(init .)
      git %(add .)
      git %(commit -am "Initial commit for #{name}...")
      git %(remote add origin "#{options[:remote]}") if options[:remote]
      git %(push origin master)

      options[:annotated_tags]&.each do |tag|
        File.open('tag', 'w') { |f| f.write(tag) }
        git %(add tag)
        git %(commit -am "Create tag #{tag}")
        git %(tag "#{tag}" -m "#{tag}")
        git %(push origin "#{tag}") if options[:remote]
      end

      options[:tags]&.each do |tag|
        File.open('tag', 'w') { |f| f.write(tag) }
        git %(add tag)
        git %(commit -am "Create tag #{tag}")
        git %(tag "#{tag}")
        git %(push origin "#{tag}") if options[:remote]
      end

      options[:branches]&.each do |branch|
        git %(checkout -b #{branch} master)
        File.open('branch', 'w') { |f| f.write(branch) }
        git %(add branch)
        git %(commit -am "Create branch #{branch}")
        git %(push origin "#{branch}") if options[:remote]
        git %(checkout master)
      end
    end

    path
  end

  def remote_git_repo(name, options = {})
    path = File.join(remotes, name)
    remote_url = "file://#{path}"
    options[:remote] = remote_url
    # Create a bogus software
    FileUtils.mkdir_p(path)

    Dir.chdir(path) do
      git %(init --bare)
      git %(config core.sharedrepository 1)
      git %(config receive.denyNonFastforwards true)
      git %(config receive.denyCurrentBranch ignore)
    end

    local_git_repo(name, options)
    path
  end

  def remotes
    path = File.join(tmp_path, 'git', 'remotes')
    FileUtils.mkdir_p(path) unless File.directory?(path)
    path
  end

  def tmp_path
    File.expand_path('../tmp', __dir__)
  end
end
