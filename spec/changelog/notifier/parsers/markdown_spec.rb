# frozen_string_literal: true

RSpec.describe Changelog::Notifier::Parsers::Markdown do
  context 'when passing a complete and valid markdown' do
    let(:parser_instance) do
      described_class.new(
        <<~HEREDOC
          # Changelog
          All notable changes to this project will be documented in this file.

          The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
          and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

          ## [Unreleased]

          ## [1.0.0] - 2017-06-20
          ### Added
          - New visual identity by [@tylerfortune8](https://github.com/tylerfortune8).
          - Version navigation.
          - Links to latest released version in previous versions.

          ### Changed
          - Start using "changelog" over "change log" since it's the common usage.
          - Start versioning based on the current English version at 0.3.0 to help
          translation authors keep things up-to-date.

          ### Removed
          - Section about "changelog" vs "CHANGELOG".

          ### Dependencies
          - Update titleize 1.4.0 -> 1.4.1

          ## [0.3.0] - 2015-12-03
          ### Added
          - RU translation from [@aishek](https://github.com/aishek).
          - pt-BR translation from [@tallesl](https://github.com/tallesl).

          [Unreleased]: https://github.com/olivierlacan/keep-a-changelog/compare/v1.0.0...HEAD
          [1.0.0]: https://github.com/olivierlacan/keep-a-changelog/compare/v0.3.0...v1.0.0
          [0.3.0]: https://github.com/olivierlacan/keep-a-changelog/compare/v0.2.0...v0.3.0
        HEREDOC
      )
    end

    context 'with an invalid version number' do
      let(:commit_tag) { 'abcd' }

      it 'raises an ArgumentError' do
        expect { parser_instance.extract(commit_tag) }
          .to raise_error(ArgumentError, 'Invalid version')
      end
    end

    context "with a version number that's not part of the CHANGELOG" do
      let(:commit_tag) { 'v2.0.0' }

      it 'raises an ReleaseNoteNotFound' do
        expect { parser_instance.extract(commit_tag) }
          .to raise_error(Changelog::Notifier::ReleaseNoteNotFound)
      end
    end

    context 'with a valid version number' do
      let(:commit_tag) { 'v1.0.0' }

      it 'returns a Hash from the given version' do
        expect(parser_instance.extract(commit_tag)).to eql(
          {
            version: '1.0.0',
            date: '2017-06-20',
            url: 'https://github.com/olivierlacan/keep-a-changelog/compare/v0.3.0...v1.0.0',
            changes: {
              added: [
                'New visual identity by [@tylerfortune8](https://github.com/tylerfortune8).',
                'Version navigation.',
                'Links to latest released version in previous versions.'
              ],
              changed: [
                "Start using \"changelog\" over \"change log\" since it's the " \
                'common usage.',
                'Start versioning based on the current English version at ' \
                '0.3.0 to help translation authors keep things up-to-date.'
              ],
              removed: [
                'Section about "changelog" vs "CHANGELOG".'
              ],
              dependencies: [
                'Update titleize 1.4.0 -> 1.4.1'
              ]
            }
          }
        )
      end
    end
  end

  context 'when passing a valid markdown but with missing version URLs' do
    let(:parser_instance) do
      described_class.new(
        <<~HEREDOC
          # Changelog
          All notable changes to this project will be documented in this file.

          The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
          and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

          ## [Unreleased]

          ## [1.0.0] - 2017-06-20
          ### Added
          - New visual identity by [@tylerfortune8](https://github.com/tylerfortune8).
          - Version navigation.
          - Links to latest released version in previous versions.

          ### Changed
          - Start using "changelog" over "change log" since it's the common usage.
          - Start versioning based on the current English version at 0.3.0 to help
          translation authors keep things up-to-date.

          ### Removed
          - Section about "changelog" vs "CHANGELOG".

          ## [0.3.0] - 2015-12-03
          ### Added
          - RU translation from [@aishek](https://github.com/aishek).
          - pt-BR translation from [@tallesl](https://github.com/tallesl).
        HEREDOC
      )
    end

    context 'with a valid version number' do
      let(:commit_tag) { 'v1.0.0' }

      it 'returns a Hash of from the given version with a nil url' do
        expect(parser_instance.extract(commit_tag)).to eql(
          {
            version: '1.0.0',
            date: '2017-06-20',
            url: nil,
            changes: {
              added: [
                'New visual identity by [@tylerfortune8](https://github.com/tylerfortune8).',
                'Version navigation.',
                'Links to latest released version in previous versions.'
              ],
              changed: [
                "Start using \"changelog\" over \"change log\" since it's the " \
                'common usage.',
                'Start versioning based on the current English version at ' \
                '0.3.0 to help translation authors keep things up-to-date.'
              ],
              removed: [
                'Section about "changelog" vs "CHANGELOG".'
              ]
            }
          }
        )
      end
    end
  end

  context 'when passing valid markdown with in-line version links' do
    let(:parser_instance) do
      described_class.new(
        <<~HEREDOC
          # Changelog
          All notable changes to this project will be documented in this file.

          The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
          and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

          ## [Unreleased]

          ## [1.0.0](https://github.com/olivierlacan/keep-a-changelog/compare/v0.3.0...v1.0.0) - 2017-06-20
          ### Added
          - New visual identity by [@tylerfortune8](https://github.com/tylerfortune8).
          - Version navigation.
          - Links to latest released version in previous versions.

          ### Changed
          - Start using "changelog" over "change log" since it's the common usage.
          - Start versioning based on the current English version at 0.3.0 to help
          translation authors keep things up-to-date.

          ### Removed
          - Section about "changelog" vs "CHANGELOG".

          ## [0.3.0] - 2015-12-03
          ### Added
          - RU translation from [@aishek](https://github.com/aishek).
          - pt-BR translation from [@tallesl](https://github.com/tallesl).
      HEREDOC
      )
    end

    context 'with a valid version number' do
      let(:commit_tag) { 'v1.0.0' }

      it 'returns a Hash of from the given version with a nil url' do
        expect(parser_instance.extract(commit_tag)).to eql(
          {
            version: '1.0.0',
            date: '2017-06-20',
            url: 'https://github.com/olivierlacan/keep-a-changelog/compare/v0.3.0...v1.0.0',
            changes: {
              added: [
                'New visual identity by [@tylerfortune8](https://github.com/tylerfortune8).',
                'Version navigation.',
                'Links to latest released version in previous versions.'
              ],
              changed: [
                "Start using \"changelog\" over \"change log\" since it's the " \
                'common usage.',
                'Start versioning based on the current English version at ' \
                '0.3.0 to help translation authors keep things up-to-date.'
              ],
              removed: [
                'Section about "changelog" vs "CHANGELOG".'
              ]
            }
          }
        )
      end
    end
  end

  context 'when parsing with section names not in English' do
    let(:parser_instance) do
      described_class.new(
        <<~HEREDOC
          # Changelog
          All notable changes to this project will be documented in this file.

          The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
          and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

          ## [Unreleased]

          ## [1.0.0](https://github.com/olivierlacan/keep-a-changelog/compare/v0.3.0...v1.0.0) - 2017-06-20
          ### Přidáno
          - New visual identity by [@tylerfortune8](https://github.com/tylerfortune8).
          - Version navigation.
          - Links to latest released version in previous versions.

          ### Změněno
          - Start using "changelog" over "change log" since it's the common usage.
          - Start versioning based on the current English version at 0.3.0 to help
          translation authors keep things up-to-date.

          ### Odstraněno
          - Section about "changelog" vs "CHANGELOG".
      HEREDOC
      )
    end

    context 'with a valid version number' do
      let(:commit_tag) { 'v1.0.0' }

      it 'returns a Hash from the given version' do
        expect(parser_instance.extract(commit_tag)).to eql(
                                                         {
                                                           version: '1.0.0',
                                                           date: '2017-06-20',
                                                           url: 'https://github.com/olivierlacan/keep-a-changelog/compare/v0.3.0...v1.0.0',
                                                           changes: {
                                                             přidáno: [
                                                               'New visual identity by [@tylerfortune8](https://github.com/tylerfortune8).',
                                                               'Version navigation.',
                                                               'Links to latest released version in previous versions.'
                                                             ],
                                                             změněno: [
                                                               "Start using \"changelog\" over \"change log\" since it's the " \
                'common usage.',
                                                               'Start versioning based on the current English version at ' \
                '0.3.0 to help translation authors keep things up-to-date.'
                                                             ],
                                                             odstraněno: [
                                                               'Section about "changelog" vs "CHANGELOG".'
                                                             ]
                                                           }
                                                         }
                                                       )
      end
    end

  end
end
