# frozen_string_literal: true

RSpec.describe Changelog::Notifier::Adapters::Slack do
  let(:capistrano) { FakeCapistrano.new }

  context 'when passing a nil as webhook_url' do
    before do
      # Wehhook URL
      allow(capistrano)
        .to receive(:fetch)
        .with(:changelog_notifier_slack_webhook_url)
        .and_return(nil)

      # Slack other variables
      allow(capistrano)
        .to receive(:fetch)
        .and_return(nil)
    end

    it "doesn't call the publish! method" do
      expect(described_class.new(capistrano)).not_to receive(:publish!)
    end
  end

  context 'when passing a webhook URL' do
    before do
      # Wehhook URL
      allow(capistrano)
        .to receive(:fetch)
        .with(:changelog_notifier_slack_webhook_url)
        .and_return('https://hooks.slack.com/services/test')

      # Channel
      allow(capistrano)
        .to receive(:fetch)
        .with(:changelog_notifier_slack_channel)
        .and_return('#general')

      # Emoji Icon
      allow(capistrano)
        .to receive(:fetch)
        .with(:changelog_notifier_slack_icon_emoji)
        .and_return(':package:')
    end

    let(:version) { '1.0.0' }
    let(:release_note_hash) do
      {
        application: 'twitter',
        version: version,
        date: '2017-06-20',
        url: nil,
        author: 'Guillaume Hain',
        changes: {
          added: [
            'New visual identity by [@tylerfortune8](https://github.com/tylerfortune8).',
            'Version navigation.',
            'Links to latest released version in previous versions.'
          ],
          changed: [
            "Start using \"changelog\" over \"change log\" since it's the " \
            'common usage.',
            'Start versioning based on the current English version at ' \
            '0.3.0 to help translation authors keep things up-to-date.'
          ],
          removed: [
            'Section about "changelog" vs "CHANGELOG".'
          ]
        }
      }
    end

    context 'without any options' do
      before do
        stub_request(:post, 'https://hooks.slack.com/services/test')
          .to_return(status: 200)

        described_class.new(capistrano).publish!(release_note_hash, version)
      end

      it 'posts a formatted version of the release note hash' do
        expect(
          a_request(:post, 'https://hooks.slack.com/services/test')
          .with(
            body: {
              'payload' => {
                channel: '#general',
                icon_emoji: ':package:',
                username: 'ChangeLog Notifier',
                text: '*Twitter* version *1.0.0* has just been released by ' \
                      '*Guillaume Hain* on the *Jun 20*.' \
                      "\n\nPlease find bellow the release note:",
                attachments: [
                  {
                    mrkdwn_in: ['text'],
                    color: '#36a64f',
                    title: 'Added',
                    fields: [
                      {
                        title: 'New visual identity by [@tylerfortune8](https://github.com/tylerfortune8).'
                      },
                      {
                        title: 'Version navigation.'
                      },
                      {
                        title: 'Links to latest released version in previous versions.'
                      }
                    ]
                  },
                  {
                    mrkdwn_in: ['text'],
                    color: '#fd9134',
                    title: 'Changed',
                    fields: [
                      {
                        title: 'Start using "changelog" over ' \
                               "\"change log\" since it's the common usage."
                      },
                      {
                        title: 'Start versioning based on the current ' \
                               'English version at 0.3.0 to help translation ' \
                               'authors keep things up-to-date.'
                      }
                    ]
                  },
                  {
                    mrkdwn_in: ['text'],
                    color: '#fc464a',
                    title: 'Removed',
                    fields: [
                      {
                        title: 'Section about "changelog" vs "CHANGELOG".'
                      }
                    ]
                  }
                ]
              }.to_json
            }
          )
        ).to have_been_made.once
      end
    end
  end
end
