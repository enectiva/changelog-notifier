# frozen_string_literal: true

require 'date'
require 'titleize'

module Changelog
  module Notifier
    module Formatters
      #
      # Format the given release note hash for ActiveRecord as a multiline text
      #
      class ActiveRecord
        def initialize(release_note_hash)
          @release_note_hash = release_note_hash
        end

        def format
          release_note = description_text + "\n\n"

          Array(@release_note_hash[:changes]).each do |change, logs|
            release_note << "#{change.capitalize}\n#{logs.join("\n")}\n\n"
          end

          # TODO : Avoids adding newlines for the last change ...
          release_note.gsub(/\n\z/, '')
        end

        private

        def description_text
          "*#{formatted_application_name}* version " \
          "*#{@release_note_hash[:version]}* has just been released by " \
          "*#{@release_note_hash[:author]}* on the " \
          "*#{formatted_release_date}*.\nPlease find bellow the release note:"
        end

        def formatted_application_name
          @release_note_hash[:application].gsub(/_/, ' ').titleize
        end

        def formatted_release_date
          Date.parse(@release_note_hash[:date]).strftime('%b %d')
        end
      end
    end
  end
end
