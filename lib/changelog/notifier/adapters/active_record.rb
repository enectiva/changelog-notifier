# frozen_string_literal: true

module Changelog
  module Notifier
    module Adapters
      #
      # Creates an ActiveRecord with the changelog
      #
      class ActiveRecord < Base
        def configured?
          @model.to_s.empty? == false
        end

        def publish!(release_note_hash, version)
          formatted_release_note = format_release_note_hash(release_note_hash)

          create_attributes = {}
          create_attributes[version_field] = version
          create_attributes[release_node_field] = formatted_release_note

          Array(@other_fields).each do |field, value|
            create_attributes[field] = value
          end

          @model.create!(create_attributes)
        end

        private

        #
        # Fetches all the configuration variables from Capistrano.
        #
        def fetches_adapter_configuration
          @model = @capistrano.fetch(:changelog_notifier_active_record_model)
          @version_field = @capistrano.fetch(
            :changelog_notifier_active_record_version_field
          )
          @release_node_field = @capistrano.fetch(
            :changelog_notifier_active_record_release_node_field
          )
          @other_fields = @capistrano.fetch(
            :changelog_notifier_active_record_other_fields
          )
        end

        def format_release_note_hash(release_note_hash)
          Changelog::Notifier::Formatters::ActiveRecord.new(release_note_hash)
                                                       .format
        end

        def release_node_field
          @release_node_field || 'release_node'
        end

        def version_field
          @version_field || 'version'
        end
      end
    end
  end
end
