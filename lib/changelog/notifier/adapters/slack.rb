# frozen_string_literal: true

require 'slack-notifier'

module Changelog
  module Notifier
    module Adapters
      #
      # Slack adapter sends the release note in a Slack channel.
      #
      class Slack < Base
        def configured?
          @webhook_url.to_s.empty? == false
        end

        def publish!(release_note_hash, version)
          formatted_release_note = format_release_note_hash(release_note_hash)

          notifier = instantiate_slack_notifier

          @capistrano.info "Posting #{version}'s release note in the " \
                           "Slack #{channel} channel using the #{icon_emoji} " \
                           'emoji icon ...'

          notifier.post formatted_release_note
        end

        private

        def channel
          @channel || '#general'
        end

        #
        # Fetches all the configuration variables from Capistrano.
        #
        def fetches_adapter_configuration
          @webhook_url = @capistrano.fetch(
            :changelog_notifier_slack_webhook_url
          )
          @channel = @capistrano.fetch(:changelog_notifier_slack_channel)
          @icon_emoji = @capistrano.fetch(:changelog_notifier_slack_icon_emoji)
        end

        def format_release_note_hash(release_note_hash)
          Changelog::Notifier::Formatters::Slack.new(release_note_hash).format
        end

        def icon_emoji
          @icon_emoji || ':package:'
        end

        def instantiate_slack_notifier
          ::Slack::Notifier.new(
            @webhook_url,
            channel: channel,
            icon_emoji: icon_emoji,
            username: 'ChangeLog Notifier'
          )
        end
      end
    end
  end
end
