# frozen_string_literal: true

module Changelog
  module Notifier
    module Entrypoints
      #
      # Tries to publish the version's release note after a successful
      # Capistrano deployment.
      #
      class Capistrano
        def initialize(instance)
          @capistrano = instance
        end

        def publish_release_note
          ensure_release_has_changelog_file &&
            fetch_version_from_git_tags_and_commit_author &&
            parse_changelog_file &&
            run_through_adapters
        end

        def self.run!(instance)
          entrypoint = Changelog::Notifier::Entrypoints::Capistrano.new(instance)
          entrypoint.publish_release_note
        end

        private

        def ensure_release_has_changelog_file
          return true if File.file?('CHANGELOG.md')

          @capistrano.warn 'You repository has no CHANGELOG.md file. ' \
                           'Skipping sending release note.'

          false
        end

        def fetch_version_from_git_tags_and_commit_author
          @git_repo_path = @capistrano.fetch(:repo_path)

          @capistrano.info "Git dir is now #{@git_repo_path}."

          fetch_version_from_git_tags && fetch_commit_author
        end

        def fetch_version_from_git_tags
          # Retrives the current's commit version tag if any
          @version = Changelog::Notifier::GitTagFetcher.fetch(
            capistrano: @capistrano,
            path: @git_repo_path
          )

          return true if @version

          @capistrano.warn 'No version tag found from the currernt commit. ' \
                           'Skipping sending release note.'

          false
        end

        def fetch_commit_author
          # Retrives the current's commit author
          @author = Changelog::Notifier::GitCommitAuthorFetcher.fetch(
            capistrano: @capistrano,
            path: @git_repo_path
          )

          return true if @author

          @capistrano.warn 'Commit author cannot be found from the currernt ' \
                           ' commit. Skipping sending release note.'

          false
        end

        def parse_changelog_file
          parser = Changelog::Notifier::Parsers::Markdown.new(
            File.read('CHANGELOG.md')
          )

          @release_note_hash = parser.extract(@version)

          @release_note_hash[:author] = @author
          @release_note_hash[:application] = @capistrano.fetch(
            :application,
            'the application'
          )

          true
        rescue ArgumentError => error
          @capistrano.error "Parsing CHANGELOG.md failed: #{error.message}. " \
                            'Skipping sending release note.'
        rescue Changelog::Notifier::ReleaseNoteNotFound
          @capistrano.error 'Parsing CHANGELOG.md failed: Missing release ' \
                            "note for version #{@version}." \
                            'Skipping sending release note.'
        end

        def run_through_adapters
          [
            Changelog::Notifier::Adapters::ActiveRecord,
            Changelog::Notifier::Adapters::Slack
          ].each do |adapter_class|
            adapter = adapter_class.new(@capistrano)

            next unless adapter.configured?

            adapter.publish!(@release_note_hash, @version)
          end
        rescue StandardError => error
          @capistrano.error error.message
        end
      end
    end
  end
end
