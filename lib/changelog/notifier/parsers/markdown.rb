# frozen_string_literal: true

module Changelog
  module Notifier
    module Parsers
      #
      # Parses the given `content` as Markdown following the specifications from
      # https://keepachangelog.com.
      #
      class Markdown
        def initialize(content)
          @content = content
        end

        def extract(version)
          version_is_valid?(version) || raise(ArgumentError, 'Invalid version')

          parse!

          version_release_note = detect_version_release_note_for(version)
          version_release_note || raise(Changelog::Notifier::ReleaseNoteNotFound)

          create_hash_from(version_release_note)
        end

        private

        def create_hash_from(version_release_note)
          sections = version_release_note.split('### ')

          # Removes trailing whitespaces
          sections = sections.map { |section| section.gsub(/\s+$/, '') }

          metadata = extract_metadata_from(sections.shift)
          return if metadata.nil?

          {
            version: metadata.version,
            date: metadata.date,
            url: metadata.url,
            changes: extract_actions_from(sections)
          }
        end

        def detect_version_release_note_for(version)
          version_number = fetch_version_number_from(version)

          @versions.detect do |changelog_version|
            changelog_version.start_with?("#{version_number}]") ||
              changelog_version.start_with?("#{version}]")
          end
        end

        def extract_actions_from(sections)
          sections.each_with_object({}) do |section, accumulator|
            type, logs = section.match(/^([[:alnum:]]+)\n(.*)/mi)
                               &.captures

            next unless type

            type = type.downcase.to_sym
            logs = logs.split(/^\s*-\s/m)
                       .reject(&:empty?)
                       .map { |log| log.strip }
                       .map { |log| log.gsub("\n", ' ') }

            accumulator[type] = logs
          end
        end

        def extract_metadata_from(section)
          captures =
            section.match(/(?<version>[\d\.]+)\](?:\((?<url>.*?)\))?\s+-\s+(?<date>[\d+-]+)/)&.named_captures
          return if captures.nil?

          metadata = Metadata.new(**captures)
          if metadata.url.blank?
            metadata.url = fetch_version_url_for metadata.version
          end
          metadata
        end

        def fetch_version_number_from(version)
          version.match(/([\d\.]+)/)&.captures&.first
        end

        def fetch_version_url_for(version)
          version_number = fetch_version_number_from(version)
          @content.match(%r{\[#{version_number}\]: (https://[\S]+)})
                 &.captures
                 &.first
        end

        #
        # Split the file on the double dashes, representing version sections.
        #
        def parse!
          @versions = @content.split(/##\s\[/)
        end

        def version_is_valid?(version)
          version =~ /[\d\.]+/
        end

        Metadata = Struct.new(:version, :date, :url, keyword_init: true)
      end
    end
  end
end
