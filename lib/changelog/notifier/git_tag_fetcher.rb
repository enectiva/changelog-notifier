# frozen_string_literal: true

module Changelog
  module Notifier
    #
    # Fetches a Git tag from the current commit being a version.
    #
    class GitTagFetcher
      def self.fetch(options = {})
        output = options[:capistrano].capture(
          "git --git-dir #{options[:path] || '.'} tag --contains HEAD"
        )

        return nil if output.empty?

        output.split("\n").detect { |tag| tag =~ /[\d\.]+/ }
      end
    end
  end
end
